import requests

data = {
    "label": "Taille Haie",
    "brand": "stihl",
}


def test_add_tool_success():
    print("adding tool to db")
    res = requests.post(url="http://127.0.0.1:5001/tool/post", json=data)
    print(res)
    print(res.request.body)
    assert res.status_code == 201


def test_get_one_tool():
    res = requests.post(url=f"http://127.0.0.1:5001/tool/get1>", json=data)
    print(res)
    print(res.status_code)
    assert res.status_code == 201


def test_link_tool_to_user(username, label, brand):
    data = {
        'username': username,
        'label': label,
        'brand': brand,
        'year': 2002,
        'condition': 'Mediocre'
    }

    res = requests.post(url=f"http://127.0.0.1:5001/tool/link", json=data)
    print(res)
    print(res.status_code)
    print(res.request.body)
    assert res.status_code == 201


if __name__ == '__main__':
    test_add_tool_success()
    # test_get_one_tool()
    test_link_tool_to_user('dreadfulbob2', **data)
