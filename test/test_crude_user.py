import requests

data = {
    "email": "test2@test.flask",
    "username": "dreadfulbob2",
    "password": "password",
    "first_name": "bob",
    "last_name": "lebrico",
    "address": "rue du brico",
    "birthdate": "1997-04-07",
}


def test_add_user_success():
    res = requests.post(url="http://127.0.0.1:5001/user/register", json=data)
    print(res)
    print(res.headers)
    print(res.request.body)
    print(res.request.hooks)
    assert res.status_code == 201
    return True


def test_login_user():
    print("loging in with previously created user")
    res = requests.post(url="http://127.0.0.1:5001/user/login", json=data)
    print(res)
    print(res.headers)
    print(res.request.body)
    print(res.status_code)
    print(res.json())
    assert res.status_code == 200
    return res


def test_get_users():
    pass


def test_delete_user():  # Requires admin rights
    pass


if __name__ == '__main__':
    test_add_user_success()
    test_login_user()
