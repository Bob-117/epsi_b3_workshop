from wsgiref.validate import validator

from src.model.user import UserModel as User


class UserService:
    @staticmethod
    def entry_check(label):
        # return type(price) is float or type(price) is int and type(name) is str
        return type(label) is str

    @classmethod
    def check_json(cls, _json):
        print(_json)
        print(_json['email'], _json['first_name'], _json['last_name'],
              _json['first_name'], _json['password'])

        return True

    @classmethod
    def exists(cls, email, username, **extra):
        try:
            print(f"checking existence for {email} + {username}")
            return User.query \
                .filter_by(email=email, username=username) \
                .first()
        except Exception as e:
            print(e)
            return False
