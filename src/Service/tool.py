from src.model.tool import ToolModel


class ToolService:

    @staticmethod
    def brand_exists(label):
        return ToolModel.query.filter_by(label=label).first() is not None

    @staticmethod
    def tool_exists(brand):
        return ToolModel.query.filter_by(brand=brand).first() is not None

    @staticmethod
    def does_not_exist(label, brand, **extra):
        brand_ok = not ToolService.brand_exists(brand)
        label_ok = not ToolService.tool_exists(label)
        try:
            return brand_ok and label_ok or \
                   not brand_ok and label_ok or \
                   not label_ok and brand_ok
        except Exception as e:
            return e

    @classmethod
    def entry_check(cls, label, brand, **extra):
        return type(label) is str and \
               type(brand) is str

    @classmethod
    def available(cls, tool):
        return tool.available
