from src.model.Item import ItemModel


class ItemService:
    @staticmethod
    def does_not_exist(search_label):
        try:
            return ItemModel.query.filter_by(label=search_label).first() is None
        except Exception as e:
            return e

    @classmethod
    def entry_check(cls, label):
        # return type(price) is float or type(price) is int and type(name) is str
        return type(label) is str
