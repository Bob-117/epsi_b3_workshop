# -*- coding: utf-8 -*-
import os
from types import MappingProxyType
from typing import Final

import coloredlogs as coloredlogs

version = (1, 0, 0)

SERVER_HOST = '0.0.0.0'  # Allows distant access
SERVER_PORT = '5001'

DATABASE_NAME = os.getenv("DATABASE_NAME", "workshop_db")
DATABASE_USER = os.getenv("DATABASE_USER", "user")
DATABASE_PASS = os.getenv("DATABASE_PASS", "password")
DATABASE_HOST = os.getenv("DATABASE_HOST", "localhost")
DATABASE_PORT = os.getenv("DATABASE_PORT", "5432")
if not os.getenv("SECRET_KEY"):
    os.putenv("SECRET_KEY", str(os.urandom(30)))
SECRET_KEY = os.getenv("SECRET_KEY")
# Warning ! if this key changes, you may not be able to use certain services like Bcrypt

VERBOSITY_COUNT_TO_LEVEL: Final = MappingProxyType({
    0: "CRITICAL",
    1: "ERROR",
    2: "WARNING",
    3: "INFO",
    4: "DEBUG",
})

DEBUG: Final = VERBOSITY_COUNT_TO_LEVEL[4]  # prod config
coloredlogs.install(DEBUG)
