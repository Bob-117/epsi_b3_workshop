# -*- coding: utf-8 -*-

__version__ = '1.0'

# Import des bibliotheques standard
import logging
import re

import sqlalchemy
from flask import Flask
from flask_bcrypt import Bcrypt
from flask_cors import CORS

# Import des ressources custom
from flask_login import LoginManager
from flask_migrate import Migrate
from src.conf import Conf
from src.conf.init_server import db, ma
from src.route.Item import item_api

# Class
from src.route.tool import tool_api
from src.route.user import user_api

logger = logging.getLogger(__name__)


class Server:
    """
    Objet Server, librairie Flask
    Implementation de route d'API REST

    :ivar _conf: configuration reseau (ip, port) sur lequel le serveur ecoute
    """

    ###################### CONSTRUCTOR ######################
    def __init__(self):
        """
        Instancie un nouvel objet serveur
        Possede un attribut database qui est une instance de la classe Database
        """
        self.__host = Conf.SERVER_HOST
        self.__port = Conf.SERVER_PORT

    def create(self):
        app = Flask(__name__)

        # some conf
        app.config['DEBUG'] = True
        app.config['CORS_HEADER'] = 'Content-Type'
        app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
        app.config['SECRET_KEY'] = Conf.SECRET_KEY
        app.config["SQLALCHEMY_DATABASE_URI"] = \
            f"postgresql://{Conf.DATABASE_USER}:{Conf.DATABASE_PASS}" \
            f"@{Conf.DATABASE_HOST}:{Conf.DATABASE_PORT}/{Conf.DATABASE_NAME}"
        login_manager = LoginManager()
        login_manager.session_protection = "strong"
        login_manager.login_view = "login"
        login_manager.login_message_category = "info"
        migrate = Migrate()
        bcrypt = Bcrypt()

        # apps linking
        db.init_app(app)
        bcrypt.init_app(app)
        ma.init_app(app)
        migrate.init_app(app, db)
        login_manager.init_app(user_api)
        login_manager.init_app(app)

        app.app_context().push()
        # user_manager.init_app(app, db, User)
        # exporting context for further use in linking other apis

        # registering apps to server
        app.register_blueprint(item_api, url_prefix="/item")
        app.register_blueprint(user_api, url_prefix="/user")
        app.register_blueprint(tool_api, url_prefix="/tool")

        # db.create_engine("postgres://user:password@127.0.0.1:5432/postgres")
        # engine = sqlalchemy.create_engine("postgresql://user:password@127.0.0.1:5432/workshop_db")
        # conn = engine.connect()
        # # conn.execute("DROP DATABASE IF EXISTS workshop_db;")
        # # conn.execute("CREATE DATABASE workshop_db;")
        # conn.close()

        db.create_all()

        # Pour chaque api blueprint, si au moins l'une n'existe pas, crée toutes les tables
        # for table in app.blueprints:
        #     target = re.sub("api", "table", table)
        #     if not (db.inspect(db.engine).has_table(target)):
        #         db.create_all()
        #         break

        # CORS(app)  # Cross-origin ressources
        cors = CORS(app, resources={r"/*": {"origins": "*"}})

        # CORS(app, origins=["localhost"], supports_credentials=True)  # Cross-origin ressources
        return app

    def start(self):
        """
        Method generale de fonctionnement et de demarrage du serveur
        On y definit ici les routes et les reponses associees
        """
        logger.warning("***start workshop***")

        main = self.create()
        main.run(host=self.__host, port=self.__port)
        # server start => create one table if not exists
        # self.get_db().create_table()
        # self.http_request()

        ############# ROUTES #############
        # # TODO : return response.status
        # @app.route('/')
        # def home():
        #     return "root"
        #
        # # end home
        #
        #
        # ############# MAIN COMMAND #############
        # app.

    # def get_db(self):
    #     return self.__db

    # def save(self, new_item: Item):
    #     # TODO : verifier la sauvegarde et renvoyer le nouvel objet si creation
    #     """
    #     Enregistrer un nouvel objet en base de donnees (suite a une requete post ligne 62)
    #     :param Item new_item:
    #     :return:
    #     """
    #     self.get_db().add_item(new_item)
    #
    # # end save
    # def http_request(self):
    #     print("**********DEBUG***************")
    #     req = HttpRequest()
    #     # req.select_query()
    #     print("******************************")
    #
