from time import sleep

from sqlalchemy import create_engine


class MYSQLDatabase:

    def __init__(self, host, port):
        self.host = host
        self.port = port


    def get_connection(self):
        """
        Ouverture d'une connexion a la base de donnees (parametres dans Conf.py et Vault.py)
        :return:
        """
        engine = None
        while engine is None:
            print("***DB connection***")
            try:
                # res = psycopg2.connect(
                #     host=self.__dbHost,
                #     port=self.__dbPort,
                #     user=self.__dbUser,
                #     password=self.__dbPassword,
                # )

                # dialect + driver: // username: password @ host:port / database
                engine = create_engine("mysql://root:pass@localhost:3306/workshop_db", echo=True, future=True)
            except Exception as e:
                print(e)
            sleep(2)
        return engine

    def show(self):
        print(f"{self.port}")
