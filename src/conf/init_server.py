from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy

########################################################################################################################
# Init

db = SQLAlchemy()
ma = Marshmallow()
