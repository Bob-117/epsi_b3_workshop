from flask_marshmallow import Schema
from marshmallow.fields import Integer, Str, Float, Dict

from src.conf.init_server import ma
from src.model.tool import ToolModel


class ToolSchema(Schema):
    class Meta:
        model = ToolModel
        fields = ["label", "brand", "year", "condition"]
        label = Str()
        brand = Str()
        year = Integer()
        condition = Str()


class ToolSchemaCreationIn(Schema):
    model = ToolModel
    fields = ["label", "brand", "year", "condition"]
    label = Str()
    brand = Str()
    year = Integer()
    condition = Str()


class ToolSchemaCreationSuccess(Schema):
    class Meta:
        fields = ["status", "msg", "new_created"]

    status = Integer(example=201)
    msg = Str(example="Tool successfully added")
    new_created = Integer(example=17)
