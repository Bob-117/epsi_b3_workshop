from flask_marshmallow import Schema
from marshmallow.fields import Integer, Str, Float, Dict
from src.model.user import UserModel as User


class UserSchema(Schema):
    """
    class ItemSchema
    """

    class Meta:
        model = User
        fields = ["id", "email", "username"]
        label = Str()


class UserSchemaCreationIn(Schema):
    model = User
    fields = ["email", "username", "password", "first_name", "last_name", "address", "birthdate"]
    label = Str()


class UserSchemaCreationSuccess(Schema):
    """
    class ItemSchemaCreationSuccess
    """

    class Meta:
        """
        Classe Meta de ItemSchemaCreationSuccess
        """
        # Champs à exposer
        fields = ["status", "msg", "new_created"]

    # end class Meta
    status = Integer(example=201)
    msg = Str(example="created")
    new_created = Integer(example=17)
