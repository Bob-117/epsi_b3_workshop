from flask_marshmallow import Schema
from marshmallow.fields import Integer, Str, Float, Dict

from src.conf.init_server import ma
from src.model.Item import ItemModel


class ItemSchema(Schema):
    """
    class ItemSchema
    """
    class Meta:
        model = ItemModel
        fields = ["label"]
        label = Str()


class ItemSchemaCreationIn(Schema):
    model = ItemModel
    fields = ["label"]
    label = Str()


class ItemSchemaCreationSuccess(Schema):
    """
    class ItemSchemaCreationSuccess
    """

    class Meta:
        """
        Classe Meta de ItemSchemaCreationSuccess
        """
        # Champs à exposer
        fields = ["status", "msg", "new_created"]

    # end class Meta
    status = Integer(example=201)
    msg = Str(example="created")
    new_created = Integer(example=17)
