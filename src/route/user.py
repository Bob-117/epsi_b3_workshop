import logging
from datetime import datetime, timedelta

import jwt
from flask import Blueprint, jsonify, current_app, make_response
from flask import request
from flask_cors import CORS

from src.Service.user import UserService
from src.conf.Server import db
from src.model.tool import ToolModel
from src.model.user import UserModel as User
from src.shema.user import UserSchema

user_api = Blueprint('user_api', __name__)
CORS(user_api, supports_credentials=True)
logger = logging.getLogger(__name__)


# @app.route('/friendList<int:page>', methods=['GET', 'POST'])
# @app.route('/friends')
@user_api.route('/get<user_id>', methods=('GET',))
@user_api.route('/get')
def get_user(user_id=0):
    logger.info("get_user")
    try:
        user_list_model = User.query.all() if not user_id else User.query.filter_by(
            id=user_id).all()
        item_list_schema = UserSchema(many=True)
        return item_list_schema.dump(user_list_model)
    except Exception as e:
        logger.error(e)
        return {"msg": "error getting item data from db"}, 401


# The User page is accessible to authenticated users (users that have logged in)
@user_api.route('/login', methods=('POST',))
def login():
    auth = request.authorization
    post = request.get_json()
    logger.info(post)
    if not post or not post['email'] or not post['password']:
        return make_response('could not verify', 401, {'Authentication': 'login required"'})

    user = User.query.filter_by(email=post['email']).first()
    logger.info(user)
    if user:
        if user.authenticate(**post):
            token = jwt.encode({
                'sub': f"{user.username} : {user.email}",
                'iat': datetime.utcnow(),
                'exp': datetime.utcnow() + timedelta(minutes=30)},
                str(current_app.config['SECRET_KEY']))
            # login_user(user)  # Requires login_manager to be correctly loaded.
            logger.info("User logged in")
            return jsonify({'logged': True, 'token': token, 'hehe': "hazja"}), 200

    logger.error(f"could not log in {post}")
    return make_response('could not verify', 401, {'Authentication': '"login required"'})


@user_api.route('/register', methods=["POST"])
def register():
    logger.info(request)
    logger.info(request.get_json(force=True))
    data = request.get_json()
    logger.info(data)
    if UserService.exists(**data):
        logger.error("already there")
        return make_response('invalid fields', 401, {'Register': '"login required"'})

    logger.info("ok")
    user = User(**data)
    db.session.add(user)
    db.session.flush()
    db.session.commit()
    return jsonify(user.to_dict()), 201


@user_api.route('/profile', methods=('GET', 'POST'))
def user_profile_page():
    if request.method == 'GET':
        return make_response({'msg': 'current user'})
    if request.method == 'POST':
        return make_response('changing profile', 202,
                             {'msg': f"changed profile for current user"})


@user_api.route('/<tool_id>', methods=('GET',))
def get_tool_users(tool_id):
    logger.info("get_tool_by_users")
    tool = ToolModel.query.filter_by(id=tool_id).all()
    if not tool:
        return make_response('invalid tool', 404, {'Lookup': '"tool not found"'})
    logger.info(tool)

    users = User.query.join('tools').filter_by(tool_id).all()
    logger.info(users)
    if not users:
        logger.error("no user has this tool")
        return make_response('no user has this tool', 404, {'Lookup': '"no users for this tool"'})

    logger.info("OK")
    return jsonify(users.to_dict()), 200


@user_api.route('/<username>/tools', methods=('GET',))
def get_user_tools(username):
    logger.info("get_user_tools")
    user = User.query.filter_by(username=username).first()
    if not user:
        return make_response('invalid user', 404, {'Lookup': '"username not found"'})
    logger.info(user)

    tools = user.query.join('tool_table').all()
    logger.info(tools)
    if not tools:
        logger.error("User has no tool")
        return make_response('User has no tools', 404, {'Lookup': '"no tools for this user"'})

    logger.info("OK")
    return jsonify([tool.to_dict() for tool in tools]), 201
