import logging

from flask import Blueprint, request
from flask_cors import CORS

from src.Service.Item import ItemService
from src.conf.init_server import db
from src.model.Item import ItemModel
from src.shema.Item import ItemSchema, ItemSchemaCreationSuccess

item_api = Blueprint("item_api", __name__)
CORS(item_api, supports_credentials=True)


@item_api.route("/get", methods=["GET"])
def get_item():
    logging.getLogger("ROUTE GET").error(">>>>>>>>>>>>>>>get")
    try:
        item_list_model = ItemModel.query.all()
        item_list_schema = ItemSchema(many=True)
        return item_list_schema.dump(item_list_model)
    except Exception as e:
        print(e)
        return {"msg": "error getting item data from db"}


@item_api.route("/post", methods=["POST"])
def post_item():
    print(request.get_json())
    logging.getLogger("ROUTE POST").error(">>>>>>>>>>>>>>>>>>post")

    new_item_label = request.get_json()["label"]

    if not ItemService.entry_check(label=new_item_label):
        return {"msg": "bad entry"}
    # end if
    if ItemService.does_not_exist(new_item_label):
        new_item_model = ItemModel(label=new_item_label)
        db.session.add(new_item_model)
        db.session.flush()
        db.session.commit()
    # return f"post {new_item_model.id}"
        data_status = 201
        data_msg = "creation success"
        data_new_created = new_item_model.id
    # end if

    else:
        data_status = 409
        data_msg = "conflict, already exists"
        data_new_created = -1
    # end else
    #
    output_schema = ItemSchemaCreationSuccess()
    data = {
        "status": data_status,
        "msg": data_msg,
        "new_created": data_new_created
    }
    return output_schema.load(data)
