import logging

from flask import Blueprint, request, make_response
from flask_cors import CORS

from src.conf.init_server import db
from src.Service.tool import ToolService
from src.model.tool import ToolModel
from src.model.user import UserModel as User, UserTools
from src.shema.tool import ToolSchema, ToolSchemaCreationSuccess

tool_api = Blueprint("tool_api", __name__)
CORS(tool_api, supports_credentials=True)
logger = logging.getLogger(__name__)


@tool_api.route("/get<int:tool_id>", methods=["GET"])
@tool_api.route("/get")
def get_tool(tool_id=0):
    logging.getLogger("ROUTE GET").error(">>>>>>>>>>>>>>>get")
    try:
        tool_list_model = \
            ToolModel.query.all() if not tool_id \
                else ToolModel.query.filter_by(id=tool_id)

        tool_list_schema = ToolSchema(many=True)
        return tool_list_schema.dump(tool_list_model), 200
    except Exception as e:
        logger.error(e)
        return {"msg": "No trailer data found in database"}


@tool_api.route("/get<int:tool_id>", methods=["GET"])
@tool_api.route("/users")
def get_user_tool(tool_id=0):
    try:
        tool_list_model = \
            ToolModel.query.all() if not tool_id \
                else ToolModel.query.filter_by(id=tool_id)

        tool_list_schema = ToolSchema(many=True)
        return tool_list_schema.dump(tool_list_model), 200
    except Exception as e:
        logger.error(e)
        return {"msg": "No trailer data found in database"}


@tool_api.route("/post", methods=["POST"])
def post_tool():
    logger.info(request.get_json())
    logger.error(">>>>>>>>>>>>>>>>>>post")
    data = request.get_json()
    if not ToolService.entry_check(**data):
        return {"msg": "bad entry"}
    # end if
    if ToolService.does_not_exist(**data):
        new_tool_model = ToolModel(**data)
        db.session.add(new_tool_model)
        db.session.flush()
        db.session.commit()
        data_status = 201
        data_msg = "New tool added"
        data_new_created = new_tool_model.id

    else:
        data_status = 409
        data_msg = "This tool already exists !"
        data_new_created = -1
    output_schema = ToolSchemaCreationSuccess()
    data = {
        "status": data_status,
        "msg": data_msg,
        "new_created": data_new_created
    }
    return output_schema.load(data), data_status


@tool_api.route("/link", methods=("POST",))
def link_tool_to_user():
    logger.info(request.get_json())
    data = request.get_json()
    email = data.get('username')
    tool = data.get('tool')

    logger.info(email)
    user = User.query.filter_by(email=email).first()
    tool = ToolModel.query.filter_by(label=tool['label'], brand=tool['brand']).first()
    logger.info(f"user : {user}, tool : {tool}")
    if tool and user:  # TODO SERVICE.if_not_exist(user tool date)
        new_link = UserTools(user, tool)
        db.session.add(new_link)
        db.session.flush()
        db.session.commit()
        data_status = 201
        data_msg = "New link added"
        data_new_created = new_link.id

    else:
        data_status = 409
        data_msg = "This tool is not yet available !"
        data_new_created = -1
    output_schema = ToolSchemaCreationSuccess()
    data = {
        "status": data_status,
        "msg": data_msg,
        "new_created": data_new_created
    }
    return output_schema.load(data), data_status


@tool_api.route("/reserve", methods=("PUT",))
def reserve_tool():
    # user, tool,
    logger.info("reserve tool")
    current_id = request.get_json()['id']
    current_asking = request.get_json()['asking_user']
    # print("reserving tool")
    # current_user = request.get_json()['obj']['user']
    # current_asking = request.get_json()['obj']["asking_user"]
    # current_tool = request.get_json()['obj']["tool"]['brand']
    # print(f'user: { current_user }  asking : { current_asking} tool : { current_tool }')
    # current_offer = UserTools.query.filter_by(asking_user=current_asking, user=current_user)
    # print(current_offer)
    current_offer = UserTools.query.filter_by(id=current_id).first()
    current_offer.status = "Pending"
    current_offer.asking_user = current_asking
    logger.info(current_offer)
    db.session.add(current_offer)
    db.session.commit()
    db.session.flush()
    logger.info("reserve tool OK")

    # current_item = UserTools.query.filter_by(id=current_id).first()
    #
    # current_item.status = "BOBBY"
    # db.session.add(current_item)
    # db.session.commit()
    # db.session.flush()
    # output_data = {
    #     "status": 200,
    #     "msg": msg,
    #     "updated": {
    #         "current_id": current_item.id,
    #         "old_value": temp_current_name,
    #         "new_value": current_item.name,
    #     }
    # }
    # return ItemSchemaUpdateOut().load(data=output_data)
    # print(current_item.id)
    return ({
        "status": 203,
        "msg": "reservation de X au nom de Y le 17/17/1117",
        "update": current_offer.id
    })


@tool_api.route('/linked', methods=('GET',))
def get_linked_tools():
    # date, user, tools
    logger.info(f"Adding a tool to user's list")
    items = UserTools.query.all()
    logger.info(items)
    if not items:
        logger.error(f"No such item")
        return make_response('User has no tools', 404, {'Lookup': '"no tools for this user"'})
    logger.info(f"Generating item list...")
    response_body = []
    offer: UserTools
    for offer in items:
        dict = offer.to_dict()
        username = dict["user"].username
        tool = dict["tool"].to_dict()
        resp = {'user': username, 'tool': tool, 'status': offer.status,
                'condition': offer.condition, 'asking_user': offer.asking_user, 'id': offer.id}
        response_body.append(resp)

    logger.info(response_body)
    return make_response(
        {'state': 'DONE', 'status': 202, 'response': response_body})
