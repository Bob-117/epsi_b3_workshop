from src.conf.init_server import db


class ToolModel(db.Model):
    __tablename__ = "tool_table"
    id = db.Column("id", db.Integer, primary_key=True, autoincrement=True)
    label = db.Column("label", db.String)
    brand = db.Column("brand", db.String)

    # category = db.relation("category_table.id", db.Integer, ondelete='NO ACTION')

    def __init__(self, label, brand, category=None, **extra):
        self.label = label
        self.brand = brand

    def to_dict(self):
        return {'id': self.id, 'label': self.label, 'brand': self.brand}
