"""
models user related

"""
import logging
from datetime import datetime

from flask import current_app
from flask_user import UserMixin
from sqlalchemy import Table
from werkzeug.security import generate_password_hash, check_password_hash

# Define User data-model
from src.conf.Server import db

# ASSOCIATION TABLE user_has_tool
from src.model.tool import ToolModel

logger = logging.getLogger(__name__)


# # ASSOCIATION TABLE user_has_skills
# user_has_skills = Table(
#     "user_has_skill",
#     db.metadata,
#     db.Column("user_id", db.Integer, db.ForeignKey("user_table.id"), primary_key=True),
#     db.Column("skill_id", db.Integer, db.ForeignKey("skill_table.id"), primary_key=True)
# )


class UserModel(db.Model, UserMixin):
    __tablename__ = 'user_table'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)

    # User Authentication fields
    email = db.Column(db.String(255), nullable=False, unique=True)
    email_confirmed_at = db.Column(db.DateTime)
    username = db.Column(db.String(50), nullable=False, unique=True)
    password = db.Column(db.String(255), nullable=False)

    # User fields
    active = db.Column(db.Boolean, default=True)
    first_name = db.Column(db.String(50), nullable=True)
    last_name = db.Column(db.String(50), nullable=True)
    address = db.Column(db.String(250), nullable=True)
    birthdate = db.Column(db.Date, nullable=True)

    # Relationships
    tools = db.relationship('ToolModel',
                            secondary='user_tool_table',
                            backref=db.backref('users_table.id', lazy='dynamic')
                            )

    roles = db.relationship('Role', secondary='user_roles_table',
                            backref=db.backref('users_table.id', lazy='dynamic'))

    # items = db.Column(db.Integer(), secondary='user_has_item_table', )

    # surveys = db.relationship('Survey', backref="creator", lazy=False)
    # skills = db.relationship('skill_table', secondary='user_has_skill',
    #                          backref=db.backref('users_table.id', lazy='dynamic'))

    def __init__(self, email, username, password, first_name, last_name, address, birthdate,
                 **extra):
        self.email = email
        self.username = username
        self.password = generate_password_hash(password)
        self.first_name = first_name
        self.last_name = last_name
        self.address = address
        self.birthdate = birthdate

    @classmethod
    def authenticate(cls, **kwargs):
        logger.info(f"authenticating user with {kwargs}")
        email = kwargs.get('email')
        password = kwargs.get('password')

        if not email or not password:
            logger.info("email + pass invalid")
            return None

        user = cls.query.filter_by(email=email).first()
        if not user or not check_password_hash(user.password, password):
            logger.info("email + pass invalid")
            return None

        logger.info(f"successfully logged in {user}")
        return user

    def to_dict(self):
        return dict(id=self.id, email=self.email, username=self.username)

    def get_tools(self):
        return self.tools


class UserProfile(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)

    pass


class UserTools(db.Model):
    __tablename__ = 'user_tool_table'
    id = db.Column(db.Integer(), primary_key=True, autoincrement=True)
    user_id = db.Column(db.Integer(), db.ForeignKey('user_table.id', ondelete='CASCADE'))
    tool_id = db.Column(db.Integer(), db.ForeignKey('tool_table.id', ondelete='CASCADE'))
    year = db.Column("year", db.Integer(), nullable=False)
    condition = db.Column("condition", db.String(), nullable=False)
    status = db.Column(db.String(), nullable=False, server_default=u't')
    added = db.Column("added on", db.DateTime(), nullable=False,
                      server_default=f"{datetime.now()}")
    asking_user = db.Column("asking_user", db.String(), server_default="None")

    def __init__(self, user, tool, **extra):
        self.user_id = user.id
        self.tool_id = tool.id
        self.year = extra.get('year') or '1995'
        self.condition = extra.get('condition') or 'Good'
        self.status = extra.get('status') or 'Open'
        self.added = datetime.now()
        self.asking_user = extra.get('asking_user') or "bobby"

    def to_dict(self):
        current_user = UserModel.query.filter_by(id=self.user_id).first()
        current_tool = ToolModel.query.filter_by(id=self.tool_id).first()
        return {'user': current_user, "tool": current_tool, "status": self.status,
                "condition": self.condition, 'asking_user': self.asking_user, 'id': self.id}


# Define the Role data model
class Role(db.Model):
    __tablename__ = 'roles_table'
    id = db.Column(db.Integer(), primary_key=True, autoincrement=True)
    name = db.Column(db.String(50), nullable=False, server_default=u'user',
                     unique=True)  # for @roles_accepted()
    label = db.Column(db.Unicode(255), server_default=u'')  # for display purposes


# Define the UserRoles association model
class UsersRoles(db.Model):
    __tablename__ = 'user_roles_table'
    id = db.Column(db.Integer(), primary_key=True, autoincrement=True)
    user_id = db.Column(db.Integer(), db.ForeignKey('user_table.id', ondelete='CASCADE'))
    role_id = db.Column(db.Integer(), db.ForeignKey('roles_table.id', ondelete='CASCADE'))
