from src.conf.init_server import db


# ITEM
class SkillModel(db.Model):
    __tablename__ = "skill_table"
    id = db.Column("id", db.Integer, primary_key=True, autoincrement=True)
    label = db.Column("label", db.String)

    def __init__(self, label):
        self.label = label
